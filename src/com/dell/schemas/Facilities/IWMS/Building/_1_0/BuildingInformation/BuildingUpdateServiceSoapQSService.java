/**
 * BuildingUpdateServiceSoapQSService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation;

public interface BuildingUpdateServiceSoapQSService extends javax.xml.rpc.Service {
    public java.lang.String getBuildingUpdateServiceSoapQSPortAddress();

    public com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation.IBuildingUpdateService getBuildingUpdateServiceSoapQSPort() throws javax.xml.rpc.ServiceException;

    public com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation.IBuildingUpdateService getBuildingUpdateServiceSoapQSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
