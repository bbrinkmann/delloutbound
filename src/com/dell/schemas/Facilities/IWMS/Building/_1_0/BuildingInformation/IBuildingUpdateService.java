/**
 * IBuildingUpdateService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation;

public interface IBuildingUpdateService extends java.rmi.Remote {
    public void buildingUpdate(com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation.BuildingInformation parameters) throws java.rmi.RemoteException;
}
