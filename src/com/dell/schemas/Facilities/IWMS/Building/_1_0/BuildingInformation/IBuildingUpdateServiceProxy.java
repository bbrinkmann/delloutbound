package com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation;

public class IBuildingUpdateServiceProxy implements com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation.IBuildingUpdateService {
  private String _endpoint = null;
  private com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation.IBuildingUpdateService iBuildingUpdateService = null;
  
  public IBuildingUpdateServiceProxy() {
    _initIBuildingUpdateServiceProxy();
  }
  
  public IBuildingUpdateServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initIBuildingUpdateServiceProxy();
  }
  
  private void _initIBuildingUpdateServiceProxy() {
    try {
      iBuildingUpdateService = (new com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation.BuildingUpdateServiceSoapQSServiceLocator()).getBuildingUpdateServiceSoapQSPort();
      if (iBuildingUpdateService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)iBuildingUpdateService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)iBuildingUpdateService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (iBuildingUpdateService != null)
      ((javax.xml.rpc.Stub)iBuildingUpdateService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation.IBuildingUpdateService getIBuildingUpdateService() {
    if (iBuildingUpdateService == null)
      _initIBuildingUpdateServiceProxy();
    return iBuildingUpdateService;
  }
  
  public void buildingUpdate(com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation.BuildingInformation parameters) throws java.rmi.RemoteException{
    if (iBuildingUpdateService == null)
      _initIBuildingUpdateServiceProxy();
    iBuildingUpdateService.buildingUpdate(parameters);
  }
  
  
}