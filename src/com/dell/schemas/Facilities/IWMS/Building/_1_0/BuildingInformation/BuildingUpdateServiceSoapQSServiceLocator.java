/**
 * BuildingUpdateServiceSoapQSServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation;

@SuppressWarnings("serial")
public class BuildingUpdateServiceSoapQSServiceLocator extends org.apache.axis.client.Service implements com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation.BuildingUpdateServiceSoapQSService {

    public BuildingUpdateServiceSoapQSServiceLocator() {
    }


    public BuildingUpdateServiceSoapQSServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public BuildingUpdateServiceSoapQSServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for BuildingUpdateServiceSoapQSPort
    //private java.lang.String BuildingUpdateServiceSoapQSPort_address = "https://soagw-dmz.us.dell.com/Facilities/BuildingUpdateService"; //Prod
    private java.lang.String BuildingUpdateServiceSoapQSPort_address = "https://soagw-dmznonprod.ins.dell.com/Facilities/BuildingUpdateService"; //SIT
    

    public java.lang.String getBuildingUpdateServiceSoapQSPortAddress() {
        return BuildingUpdateServiceSoapQSPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String BuildingUpdateServiceSoapQSPortWSDDServiceName = "BuildingUpdateServiceSoapQSPort";

    public java.lang.String getBuildingUpdateServiceSoapQSPortWSDDServiceName() {
        return BuildingUpdateServiceSoapQSPortWSDDServiceName;
    }

    public void setBuildingUpdateServiceSoapQSPortWSDDServiceName(java.lang.String name) {
        BuildingUpdateServiceSoapQSPortWSDDServiceName = name;
    }

    public com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation.IBuildingUpdateService getBuildingUpdateServiceSoapQSPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BuildingUpdateServiceSoapQSPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBuildingUpdateServiceSoapQSPort(endpoint);
    }

    public com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation.IBuildingUpdateService getBuildingUpdateServiceSoapQSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation.BuildingUpdateServiceSoapStub _stub = new com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation.BuildingUpdateServiceSoapStub(portAddress, this);
            _stub.setPortName(getBuildingUpdateServiceSoapQSPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBuildingUpdateServiceSoapQSPortEndpointAddress(java.lang.String address) {
        BuildingUpdateServiceSoapQSPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation.IBuildingUpdateService.class.isAssignableFrom(serviceEndpointInterface)) {
                com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation.BuildingUpdateServiceSoapStub _stub = new com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation.BuildingUpdateServiceSoapStub(new java.net.URL(BuildingUpdateServiceSoapQSPort_address), this);
                _stub.setPortName(getBuildingUpdateServiceSoapQSPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("BuildingUpdateServiceSoapQSPort".equals(inputPortName)) {
            return getBuildingUpdateServiceSoapQSPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "BuildingUpdateServiceSoapQSService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "BuildingUpdateServiceSoapQSPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("BuildingUpdateServiceSoapQSPort".equals(portName)) {
            setBuildingUpdateServiceSoapQSPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
