/**
 * BuildingInformation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation;

@SuppressWarnings("serial")
public class BuildingInformation  implements java.io.Serializable {
    private java.lang.String locationId;

    private java.util.Calendar effectiveDate;

    private java.lang.String status;

    private java.lang.String buildingName;

    private java.lang.String buildingCode;

    private java.lang.String buildingManager;

    private java.lang.String buildingType;

    private java.lang.String addressStreet1;

    private java.lang.String addressStreet2;

    private java.lang.String addressStreet3;

    private java.lang.String city;

    private java.lang.String stateName;

    private java.lang.String stateCode;

    private java.lang.String zipCode;

    private java.lang.String region;

    private java.lang.String countryName;

    private java.lang.String countryCode;

    private java.lang.String phone;

    private java.lang.String fax;

    private java.lang.String companyName;

    private java.lang.String timeZone;

    private java.lang.String property;

    public BuildingInformation() {
    }

    public BuildingInformation(
           java.lang.String locationId,
           java.util.Calendar effectiveDate,
           java.lang.String status,
           java.lang.String buildingName,
           java.lang.String buildingCode,
           java.lang.String buildingManager,
           java.lang.String buildingType,
           java.lang.String addressStreet1,
           java.lang.String addressStreet2,
           java.lang.String addressStreet3,
           java.lang.String city,
           java.lang.String stateName,
           java.lang.String stateCode,
           java.lang.String zipCode,
           java.lang.String region,
           java.lang.String countryName,
           java.lang.String countryCode,
           java.lang.String phone,
           java.lang.String fax,
           java.lang.String companyName,
           java.lang.String timeZone,
           java.lang.String property) {
           this.locationId = locationId;
           this.effectiveDate = effectiveDate;
           this.status = status;
           this.buildingName = buildingName;
           this.buildingCode = buildingCode;
           this.buildingManager = buildingManager;
           this.buildingType = buildingType;
           this.addressStreet1 = addressStreet1;
           this.addressStreet2 = addressStreet2;
           this.addressStreet3 = addressStreet3;
           this.city = city;
           this.stateName = stateName;
           this.stateCode = stateCode;
           this.zipCode = zipCode;
           this.region = region;
           this.countryName = countryName;
           this.countryCode = countryCode;
           this.phone = phone;
           this.fax = fax;
           this.companyName = companyName;
           this.timeZone = timeZone;
           this.property = property;
    }


    /**
     * Gets the locationId value for this BuildingInformation.
     * 
     * @return locationId
     */
    public java.lang.String getLocationId() {
        return locationId;
    }


    /**
     * Sets the locationId value for this BuildingInformation.
     * 
     * @param locationId
     */
    public void setLocationId(java.lang.String locationId) {
        this.locationId = locationId;
    }


    /**
     * Gets the effectiveDate value for this BuildingInformation.
     * 
     * @return effectiveDate
     */
    public java.util.Calendar getEffectiveDate() {
        return effectiveDate;
    }


    /**
     * Sets the effectiveDate value for this BuildingInformation.
     * 
     * @param effectiveDate
     */
    public void setEffectiveDate(java.util.Calendar effectiveDate) {
        this.effectiveDate = effectiveDate;
    }


    /**
     * Gets the status value for this BuildingInformation.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this BuildingInformation.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the buildingName value for this BuildingInformation.
     * 
     * @return buildingName
     */
    public java.lang.String getBuildingName() {
        return buildingName;
    }


    /**
     * Sets the buildingName value for this BuildingInformation.
     * 
     * @param buildingName
     */
    public void setBuildingName(java.lang.String buildingName) {
        this.buildingName = buildingName;
    }


    /**
     * Gets the buildingCode value for this BuildingInformation.
     * 
     * @return buildingCode
     */
    public java.lang.String getBuildingCode() {
        return buildingCode;
    }


    /**
     * Sets the buildingCode value for this BuildingInformation.
     * 
     * @param buildingCode
     */
    public void setBuildingCode(java.lang.String buildingCode) {
        this.buildingCode = buildingCode;
    }


    /**
     * Gets the buildingManager value for this BuildingInformation.
     * 
     * @return buildingManager
     */
    public java.lang.String getBuildingManager() {
        return buildingManager;
    }


    /**
     * Sets the buildingManager value for this BuildingInformation.
     * 
     * @param buildingManager
     */
    public void setBuildingManager(java.lang.String buildingManager) {
        this.buildingManager = buildingManager;
    }


    /**
     * Gets the buildingType value for this BuildingInformation.
     * 
     * @return buildingType
     */
    public java.lang.String getBuildingType() {
        return buildingType;
    }


    /**
     * Sets the buildingType value for this BuildingInformation.
     * 
     * @param buildingType
     */
    public void setBuildingType(java.lang.String buildingType) {
        this.buildingType = buildingType;
    }


    /**
     * Gets the addressStreet1 value for this BuildingInformation.
     * 
     * @return addressStreet1
     */
    public java.lang.String getAddressStreet1() {
        return addressStreet1;
    }


    /**
     * Sets the addressStreet1 value for this BuildingInformation.
     * 
     * @param addressStreet1
     */
    public void setAddressStreet1(java.lang.String addressStreet1) {
        this.addressStreet1 = addressStreet1;
    }


    /**
     * Gets the addressStreet2 value for this BuildingInformation.
     * 
     * @return addressStreet2
     */
    public java.lang.String getAddressStreet2() {
        return addressStreet2;
    }


    /**
     * Sets the addressStreet2 value for this BuildingInformation.
     * 
     * @param addressStreet2
     */
    public void setAddressStreet2(java.lang.String addressStreet2) {
        this.addressStreet2 = addressStreet2;
    }


    /**
     * Gets the addressStreet3 value for this BuildingInformation.
     * 
     * @return addressStreet3
     */
    public java.lang.String getAddressStreet3() {
        return addressStreet3;
    }


    /**
     * Sets the addressStreet3 value for this BuildingInformation.
     * 
     * @param addressStreet3
     */
    public void setAddressStreet3(java.lang.String addressStreet3) {
        this.addressStreet3 = addressStreet3;
    }


    /**
     * Gets the city value for this BuildingInformation.
     * 
     * @return city
     */
    public java.lang.String getCity() {
        return city;
    }


    /**
     * Sets the city value for this BuildingInformation.
     * 
     * @param city
     */
    public void setCity(java.lang.String city) {
        this.city = city;
    }


    /**
     * Gets the stateName value for this BuildingInformation.
     * 
     * @return stateName
     */
    public java.lang.String getStateName() {
        return stateName;
    }


    /**
     * Sets the stateName value for this BuildingInformation.
     * 
     * @param stateName
     */
    public void setStateName(java.lang.String stateName) {
        this.stateName = stateName;
    }


    /**
     * Gets the stateCode value for this BuildingInformation.
     * 
     * @return stateCode
     */
    public java.lang.String getStateCode() {
        return stateCode;
    }


    /**
     * Sets the stateCode value for this BuildingInformation.
     * 
     * @param stateCode
     */
    public void setStateCode(java.lang.String stateCode) {
        this.stateCode = stateCode;
    }


    /**
     * Gets the zipCode value for this BuildingInformation.
     * 
     * @return zipCode
     */
    public java.lang.String getZipCode() {
        return zipCode;
    }


    /**
     * Sets the zipCode value for this BuildingInformation.
     * 
     * @param zipCode
     */
    public void setZipCode(java.lang.String zipCode) {
        this.zipCode = zipCode;
    }


    /**
     * Gets the region value for this BuildingInformation.
     * 
     * @return region
     */
    public java.lang.String getRegion() {
        return region;
    }


    /**
     * Sets the region value for this BuildingInformation.
     * 
     * @param region
     */
    public void setRegion(java.lang.String region) {
        this.region = region;
    }


    /**
     * Gets the countryName value for this BuildingInformation.
     * 
     * @return countryName
     */
    public java.lang.String getCountryName() {
        return countryName;
    }


    /**
     * Sets the countryName value for this BuildingInformation.
     * 
     * @param countryName
     */
    public void setCountryName(java.lang.String countryName) {
        this.countryName = countryName;
    }


    /**
     * Gets the countryCode value for this BuildingInformation.
     * 
     * @return countryCode
     */
    public java.lang.String getCountryCode() {
        return countryCode;
    }


    /**
     * Sets the countryCode value for this BuildingInformation.
     * 
     * @param countryCode
     */
    public void setCountryCode(java.lang.String countryCode) {
        this.countryCode = countryCode;
    }


    /**
     * Gets the phone value for this BuildingInformation.
     * 
     * @return phone
     */
    public java.lang.String getPhone() {
        return phone;
    }


    /**
     * Sets the phone value for this BuildingInformation.
     * 
     * @param phone
     */
    public void setPhone(java.lang.String phone) {
        this.phone = phone;
    }


    /**
     * Gets the fax value for this BuildingInformation.
     * 
     * @return fax
     */
    public java.lang.String getFax() {
        return fax;
    }


    /**
     * Sets the fax value for this BuildingInformation.
     * 
     * @param fax
     */
    public void setFax(java.lang.String fax) {
        this.fax = fax;
    }


    /**
     * Gets the companyName value for this BuildingInformation.
     * 
     * @return companyName
     */
    public java.lang.String getCompanyName() {
        return companyName;
    }


    /**
     * Sets the companyName value for this BuildingInformation.
     * 
     * @param companyName
     */
    public void setCompanyName(java.lang.String companyName) {
        this.companyName = companyName;
    }


    /**
     * Gets the timeZone value for this BuildingInformation.
     * 
     * @return timeZone
     */
    public java.lang.String getTimeZone() {
        return timeZone;
    }


    /**
     * Sets the timeZone value for this BuildingInformation.
     * 
     * @param timeZone
     */
    public void setTimeZone(java.lang.String timeZone) {
        this.timeZone = timeZone;
    }


    /**
     * Gets the property value for this BuildingInformation.
     * 
     * @return property
     */
    public java.lang.String getProperty() {
        return property;
    }


    /**
     * Sets the property value for this BuildingInformation.
     * 
     * @param property
     */
    public void setProperty(java.lang.String property) {
        this.property = property;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BuildingInformation)) return false;
        BuildingInformation other = (BuildingInformation) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.locationId==null && other.getLocationId()==null) || 
             (this.locationId!=null &&
              this.locationId.equals(other.getLocationId()))) &&
            ((this.effectiveDate==null && other.getEffectiveDate()==null) || 
             (this.effectiveDate!=null &&
              this.effectiveDate.equals(other.getEffectiveDate()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.buildingName==null && other.getBuildingName()==null) || 
             (this.buildingName!=null &&
              this.buildingName.equals(other.getBuildingName()))) &&
            ((this.buildingCode==null && other.getBuildingCode()==null) || 
             (this.buildingCode!=null &&
              this.buildingCode.equals(other.getBuildingCode()))) &&
            ((this.buildingManager==null && other.getBuildingManager()==null) || 
             (this.buildingManager!=null &&
              this.buildingManager.equals(other.getBuildingManager()))) &&
            ((this.buildingType==null && other.getBuildingType()==null) || 
             (this.buildingType!=null &&
              this.buildingType.equals(other.getBuildingType()))) &&
            ((this.addressStreet1==null && other.getAddressStreet1()==null) || 
             (this.addressStreet1!=null &&
              this.addressStreet1.equals(other.getAddressStreet1()))) &&
            ((this.addressStreet2==null && other.getAddressStreet2()==null) || 
             (this.addressStreet2!=null &&
              this.addressStreet2.equals(other.getAddressStreet2()))) &&
            ((this.addressStreet3==null && other.getAddressStreet3()==null) || 
             (this.addressStreet3!=null &&
              this.addressStreet3.equals(other.getAddressStreet3()))) &&
            ((this.city==null && other.getCity()==null) || 
             (this.city!=null &&
              this.city.equals(other.getCity()))) &&
            ((this.stateName==null && other.getStateName()==null) || 
             (this.stateName!=null &&
              this.stateName.equals(other.getStateName()))) &&
            ((this.stateCode==null && other.getStateCode()==null) || 
             (this.stateCode!=null &&
              this.stateCode.equals(other.getStateCode()))) &&
            ((this.zipCode==null && other.getZipCode()==null) || 
             (this.zipCode!=null &&
              this.zipCode.equals(other.getZipCode()))) &&
            ((this.region==null && other.getRegion()==null) || 
             (this.region!=null &&
              this.region.equals(other.getRegion()))) &&
            ((this.countryName==null && other.getCountryName()==null) || 
             (this.countryName!=null &&
              this.countryName.equals(other.getCountryName()))) &&
            ((this.countryCode==null && other.getCountryCode()==null) || 
             (this.countryCode!=null &&
              this.countryCode.equals(other.getCountryCode()))) &&
            ((this.phone==null && other.getPhone()==null) || 
             (this.phone!=null &&
              this.phone.equals(other.getPhone()))) &&
            ((this.fax==null && other.getFax()==null) || 
             (this.fax!=null &&
              this.fax.equals(other.getFax()))) &&
            ((this.companyName==null && other.getCompanyName()==null) || 
             (this.companyName!=null &&
              this.companyName.equals(other.getCompanyName()))) &&
            ((this.timeZone==null && other.getTimeZone()==null) || 
             (this.timeZone!=null &&
              this.timeZone.equals(other.getTimeZone()))) &&
            ((this.property==null && other.getProperty()==null) || 
             (this.property!=null &&
              this.property.equals(other.getProperty())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLocationId() != null) {
            _hashCode += getLocationId().hashCode();
        }
        if (getEffectiveDate() != null) {
            _hashCode += getEffectiveDate().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getBuildingName() != null) {
            _hashCode += getBuildingName().hashCode();
        }
        if (getBuildingCode() != null) {
            _hashCode += getBuildingCode().hashCode();
        }
        if (getBuildingManager() != null) {
            _hashCode += getBuildingManager().hashCode();
        }
        if (getBuildingType() != null) {
            _hashCode += getBuildingType().hashCode();
        }
        if (getAddressStreet1() != null) {
            _hashCode += getAddressStreet1().hashCode();
        }
        if (getAddressStreet2() != null) {
            _hashCode += getAddressStreet2().hashCode();
        }
        if (getAddressStreet3() != null) {
            _hashCode += getAddressStreet3().hashCode();
        }
        if (getCity() != null) {
            _hashCode += getCity().hashCode();
        }
        if (getStateName() != null) {
            _hashCode += getStateName().hashCode();
        }
        if (getStateCode() != null) {
            _hashCode += getStateCode().hashCode();
        }
        if (getZipCode() != null) {
            _hashCode += getZipCode().hashCode();
        }
        if (getRegion() != null) {
            _hashCode += getRegion().hashCode();
        }
        if (getCountryName() != null) {
            _hashCode += getCountryName().hashCode();
        }
        if (getCountryCode() != null) {
            _hashCode += getCountryCode().hashCode();
        }
        if (getPhone() != null) {
            _hashCode += getPhone().hashCode();
        }
        if (getFax() != null) {
            _hashCode += getFax().hashCode();
        }
        if (getCompanyName() != null) {
            _hashCode += getCompanyName().hashCode();
        }
        if (getTimeZone() != null) {
            _hashCode += getTimeZone().hashCode();
        }
        if (getProperty() != null) {
            _hashCode += getProperty().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BuildingInformation.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", ">BuildingInformation"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("locationId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "LocationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("effectiveDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "EffectiveDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("buildingName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "BuildingName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("buildingCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "BuildingCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("buildingManager");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "BuildingManager"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("buildingType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "BuildingType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressStreet1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "AddressStreet1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressStreet2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "AddressStreet2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressStreet3");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "AddressStreet3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("city");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "City"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stateName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "StateName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stateCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "StateCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zipCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "ZipCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("region");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "Region"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countryName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "CountryName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "CountryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "Phone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "Fax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("companyName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "CompanyName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timeZone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "TimeZone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("property");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Building/1_0/BuildingInformation", "Property"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
