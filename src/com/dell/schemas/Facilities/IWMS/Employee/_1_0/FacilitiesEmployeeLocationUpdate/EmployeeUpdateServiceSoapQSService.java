/**
 * EmployeeUpdateServiceSoapQSService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.dell.schemas.Facilities.IWMS.Employee._1_0.FacilitiesEmployeeLocationUpdate;

public interface EmployeeUpdateServiceSoapQSService extends javax.xml.rpc.Service {
    public java.lang.String getEmployeeUpdateServiceSoapQSPortAddress();

    public com.dell.schemas.Facilities.IWMS.Employee._1_0.FacilitiesEmployeeLocationUpdate.IEmployeeUpdateService getEmployeeUpdateServiceSoapQSPort() throws javax.xml.rpc.ServiceException;

    public com.dell.schemas.Facilities.IWMS.Employee._1_0.FacilitiesEmployeeLocationUpdate.IEmployeeUpdateService getEmployeeUpdateServiceSoapQSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
