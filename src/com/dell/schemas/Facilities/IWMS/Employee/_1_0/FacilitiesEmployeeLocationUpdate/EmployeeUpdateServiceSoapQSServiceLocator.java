/**
 * EmployeeUpdateServiceSoapQSServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.dell.schemas.Facilities.IWMS.Employee._1_0.FacilitiesEmployeeLocationUpdate;

public class EmployeeUpdateServiceSoapQSServiceLocator extends org.apache.axis.client.Service implements com.dell.schemas.Facilities.IWMS.Employee._1_0.FacilitiesEmployeeLocationUpdate.EmployeeUpdateServiceSoapQSService {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public EmployeeUpdateServiceSoapQSServiceLocator() {
    }


    public EmployeeUpdateServiceSoapQSServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public EmployeeUpdateServiceSoapQSServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for EmployeeUpdateServiceSoapQSPort
    //private java.lang.String EmployeeUpdateServiceSoapQSPort_address = "https://soagw-dmz.us.dell.com/Facilities/EmployeeUpdateService"; //PROD
    private java.lang.String EmployeeUpdateServiceSoapQSPort_address = "https://soagw-dmznonprod.ins.dell.com/Facilities/EmployeeUpdateService"; //SIT
     

    public java.lang.String getEmployeeUpdateServiceSoapQSPortAddress() {
        return EmployeeUpdateServiceSoapQSPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String EmployeeUpdateServiceSoapQSPortWSDDServiceName = "EmployeeUpdateServiceSoapQSPort";

    public java.lang.String getEmployeeUpdateServiceSoapQSPortWSDDServiceName() {
        return EmployeeUpdateServiceSoapQSPortWSDDServiceName;
    }

    public void setEmployeeUpdateServiceSoapQSPortWSDDServiceName(java.lang.String name) {
        EmployeeUpdateServiceSoapQSPortWSDDServiceName = name;
    }

    public com.dell.schemas.Facilities.IWMS.Employee._1_0.FacilitiesEmployeeLocationUpdate.IEmployeeUpdateService getEmployeeUpdateServiceSoapQSPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(EmployeeUpdateServiceSoapQSPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getEmployeeUpdateServiceSoapQSPort(endpoint);
    }

    public com.dell.schemas.Facilities.IWMS.Employee._1_0.FacilitiesEmployeeLocationUpdate.IEmployeeUpdateService getEmployeeUpdateServiceSoapQSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.dell.schemas.Facilities.IWMS.Employee._1_0.FacilitiesEmployeeLocationUpdate.EmployeeUpdateServiceSoapStub _stub = new com.dell.schemas.Facilities.IWMS.Employee._1_0.FacilitiesEmployeeLocationUpdate.EmployeeUpdateServiceSoapStub(portAddress, this);
            _stub.setPortName(getEmployeeUpdateServiceSoapQSPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setEmployeeUpdateServiceSoapQSPortEndpointAddress(java.lang.String address) {
        EmployeeUpdateServiceSoapQSPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.dell.schemas.Facilities.IWMS.Employee._1_0.FacilitiesEmployeeLocationUpdate.IEmployeeUpdateService.class.isAssignableFrom(serviceEndpointInterface)) {
                com.dell.schemas.Facilities.IWMS.Employee._1_0.FacilitiesEmployeeLocationUpdate.EmployeeUpdateServiceSoapStub _stub = new com.dell.schemas.Facilities.IWMS.Employee._1_0.FacilitiesEmployeeLocationUpdate.EmployeeUpdateServiceSoapStub(new java.net.URL(EmployeeUpdateServiceSoapQSPort_address), this);
                _stub.setPortName(getEmployeeUpdateServiceSoapQSPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("EmployeeUpdateServiceSoapQSPort".equals(inputPortName)) {
            return getEmployeeUpdateServiceSoapQSPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Employee/1_0/FacilitiesEmployeeLocationUpdate", "EmployeeUpdateServiceSoapQSService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Employee/1_0/FacilitiesEmployeeLocationUpdate", "EmployeeUpdateServiceSoapQSPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("EmployeeUpdateServiceSoapQSPort".equals(portName)) {
            setEmployeeUpdateServiceSoapQSPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
