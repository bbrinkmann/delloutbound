/**
 * FacilitiesEmployeeLocationUpdate.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.dell.schemas.Facilities.IWMS.Employee._1_0.FacilitiesEmployeeLocationUpdate;

public class FacilitiesEmployeeLocationUpdate  implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private java.lang.String badge;

    private java.util.Calendar effectiveDate;

    private java.lang.String locationId;

    private java.lang.String mailStop;

    private java.lang.String useCode;

    private java.lang.String workLocation;

    private java.util.Calendar workLocationEffectiveDate;

    private java.lang.String workProfile;

    private java.util.Calendar workProfileDate;

    public FacilitiesEmployeeLocationUpdate() {
    }

    public FacilitiesEmployeeLocationUpdate(
           java.lang.String badge,
           java.util.Calendar effectiveDate,
           java.lang.String locationId,
           java.lang.String mailStop,
           java.lang.String useCode,
           java.lang.String workLocation,
           java.util.Calendar workLocationEffectiveDate,
           java.lang.String workProfile,
           java.util.Calendar workProfileDate) {
           this.badge = badge;
           this.effectiveDate = effectiveDate;
           this.locationId = locationId;
           this.mailStop = mailStop;
           this.useCode = useCode;
           this.workLocation = workLocation;
           this.workLocationEffectiveDate = workLocationEffectiveDate;
           this.workProfile = workProfile;
           this.workProfileDate = workProfileDate;
    }


    /**
     * Gets the badge value for this FacilitiesEmployeeLocationUpdate.
     * 
     * @return badge
     */
    public java.lang.String getBadge() {
        return badge;
    }


    /**
     * Sets the badge value for this FacilitiesEmployeeLocationUpdate.
     * 
     * @param badge
     */
    public void setBadge(java.lang.String badge) {
        this.badge = badge;
    }


    /**
     * Gets the effectiveDate value for this FacilitiesEmployeeLocationUpdate.
     * 
     * @return effectiveDate
     */
    public java.util.Calendar getEffectiveDate() {
        return effectiveDate;
    }


    /**
     * Sets the effectiveDate value for this FacilitiesEmployeeLocationUpdate.
     * 
     * @param effectiveDate
     */
    public void setEffectiveDate(java.util.Calendar effectiveDate) {
        this.effectiveDate = effectiveDate;
    }


    /**
     * Gets the locationId value for this FacilitiesEmployeeLocationUpdate.
     * 
     * @return locationId
     */
    public java.lang.String getLocationId() {
        return locationId;
    }


    /**
     * Sets the locationId value for this FacilitiesEmployeeLocationUpdate.
     * 
     * @param locationId
     */
    public void setLocationId(java.lang.String locationId) {
        this.locationId = locationId;
    }


    /**
     * Gets the mailStop value for this FacilitiesEmployeeLocationUpdate.
     * 
     * @return mailStop
     */
    public java.lang.String getMailStop() {
        return mailStop;
    }


    /**
     * Sets the mailStop value for this FacilitiesEmployeeLocationUpdate.
     * 
     * @param mailStop
     */
    public void setMailStop(java.lang.String mailStop) {
        this.mailStop = mailStop;
    }


    /**
     * Gets the useCode value for this FacilitiesEmployeeLocationUpdate.
     * 
     * @return useCode
     */
    public java.lang.String getUseCode() {
        return useCode;
    }


    /**
     * Sets the useCode value for this FacilitiesEmployeeLocationUpdate.
     * 
     * @param useCode
     */
    public void setUseCode(java.lang.String useCode) {
        this.useCode = useCode;
    }


    /**
     * Gets the workLocation value for this FacilitiesEmployeeLocationUpdate.
     * 
     * @return workLocation
     */
    public java.lang.String getWorkLocation() {
        return workLocation;
    }


    /**
     * Sets the workLocation value for this FacilitiesEmployeeLocationUpdate.
     * 
     * @param workLocation
     */
    public void setWorkLocation(java.lang.String workLocation) {
        this.workLocation = workLocation;
    }


    /**
     * Gets the workLocationEffectiveDate value for this FacilitiesEmployeeLocationUpdate.
     * 
     * @return workLocationEffectiveDate
     */
    public java.util.Calendar getWorkLocationEffectiveDate() {
        return workLocationEffectiveDate;
    }


    /**
     * Sets the workLocationEffectiveDate value for this FacilitiesEmployeeLocationUpdate.
     * 
     * @param workLocationEffectiveDate
     */
    public void setWorkLocationEffectiveDate(java.util.Calendar workLocationEffectiveDate) {
        this.workLocationEffectiveDate = workLocationEffectiveDate;
    }


    /**
     * Gets the workProfile value for this FacilitiesEmployeeLocationUpdate.
     * 
     * @return workProfile
     */
    public java.lang.String getWorkProfile() {
        return workProfile;
    }


    /**
     * Sets the workProfile value for this FacilitiesEmployeeLocationUpdate.
     * 
     * @param workProfile
     */
    public void setWorkProfile(java.lang.String workProfile) {
        this.workProfile = workProfile;
    }


    /**
     * Gets the workProfileDate value for this FacilitiesEmployeeLocationUpdate.
     * 
     * @return workProfileDate
     */
    public java.util.Calendar getWorkProfileDate() {
        return workProfileDate;
    }


    /**
     * Sets the workProfileDate value for this FacilitiesEmployeeLocationUpdate.
     * 
     * @param workProfileDate
     */
    public void setWorkProfileDate(java.util.Calendar workProfileDate) {
        this.workProfileDate = workProfileDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FacilitiesEmployeeLocationUpdate)) return false;
        FacilitiesEmployeeLocationUpdate other = (FacilitiesEmployeeLocationUpdate) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.badge==null && other.getBadge()==null) || 
             (this.badge!=null &&
              this.badge.equals(other.getBadge()))) &&
            ((this.effectiveDate==null && other.getEffectiveDate()==null) || 
             (this.effectiveDate!=null &&
              this.effectiveDate.equals(other.getEffectiveDate()))) &&
            ((this.locationId==null && other.getLocationId()==null) || 
             (this.locationId!=null &&
              this.locationId.equals(other.getLocationId()))) &&
            ((this.mailStop==null && other.getMailStop()==null) || 
             (this.mailStop!=null &&
              this.mailStop.equals(other.getMailStop()))) &&
            ((this.useCode==null && other.getUseCode()==null) || 
             (this.useCode!=null &&
              this.useCode.equals(other.getUseCode()))) &&
            ((this.workLocation==null && other.getWorkLocation()==null) || 
             (this.workLocation!=null &&
              this.workLocation.equals(other.getWorkLocation()))) &&
            ((this.workLocationEffectiveDate==null && other.getWorkLocationEffectiveDate()==null) || 
             (this.workLocationEffectiveDate!=null &&
              this.workLocationEffectiveDate.equals(other.getWorkLocationEffectiveDate()))) &&
            ((this.workProfile==null && other.getWorkProfile()==null) || 
             (this.workProfile!=null &&
              this.workProfile.equals(other.getWorkProfile()))) &&
            ((this.workProfileDate==null && other.getWorkProfileDate()==null) || 
             (this.workProfileDate!=null &&
              this.workProfileDate.equals(other.getWorkProfileDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBadge() != null) {
            _hashCode += getBadge().hashCode();
        }
        if (getEffectiveDate() != null) {
            _hashCode += getEffectiveDate().hashCode();
        }
        if (getLocationId() != null) {
            _hashCode += getLocationId().hashCode();
        }
        if (getMailStop() != null) {
            _hashCode += getMailStop().hashCode();
        }
        if (getUseCode() != null) {
            _hashCode += getUseCode().hashCode();
        }
        if (getWorkLocation() != null) {
            _hashCode += getWorkLocation().hashCode();
        }
        if (getWorkLocationEffectiveDate() != null) {
            _hashCode += getWorkLocationEffectiveDate().hashCode();
        }
        if (getWorkProfile() != null) {
            _hashCode += getWorkProfile().hashCode();
        }
        if (getWorkProfileDate() != null) {
            _hashCode += getWorkProfileDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FacilitiesEmployeeLocationUpdate.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Employee/1_0/FacilitiesEmployeeLocationUpdate", ">FacilitiesEmployeeLocationUpdate"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("badge");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Employee/1_0/FacilitiesEmployeeLocationUpdate", "Badge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("effectiveDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Employee/1_0/FacilitiesEmployeeLocationUpdate", "EffectiveDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("locationId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Employee/1_0/FacilitiesEmployeeLocationUpdate", "LocationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mailStop");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Employee/1_0/FacilitiesEmployeeLocationUpdate", "MailStop"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("useCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Employee/1_0/FacilitiesEmployeeLocationUpdate", "UseCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workLocation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Employee/1_0/FacilitiesEmployeeLocationUpdate", "WorkLocation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workLocationEffectiveDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Employee/1_0/FacilitiesEmployeeLocationUpdate", "WorkLocationEffectiveDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workProfile");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Employee/1_0/FacilitiesEmployeeLocationUpdate", "WorkProfile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workProfileDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.dell.com/Facilities/IWMS/Employee/1_0/FacilitiesEmployeeLocationUpdate", "WorkProfileDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
