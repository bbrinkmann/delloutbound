package com.dell.schemas.Facilities.IWMS.Employee._1_0.FacilitiesEmployeeLocationUpdate;

public class IEmployeeUpdateServiceProxy implements com.dell.schemas.Facilities.IWMS.Employee._1_0.FacilitiesEmployeeLocationUpdate.IEmployeeUpdateService {
  private String _endpoint = null;
  private com.dell.schemas.Facilities.IWMS.Employee._1_0.FacilitiesEmployeeLocationUpdate.IEmployeeUpdateService iEmployeeUpdateService = null;
  
  public IEmployeeUpdateServiceProxy() {
    _initIEmployeeUpdateServiceProxy();
  }
  
  public IEmployeeUpdateServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initIEmployeeUpdateServiceProxy();
  }
  
  private void _initIEmployeeUpdateServiceProxy() {
    try {
      iEmployeeUpdateService = (new com.dell.schemas.Facilities.IWMS.Employee._1_0.FacilitiesEmployeeLocationUpdate.EmployeeUpdateServiceSoapQSServiceLocator()).getEmployeeUpdateServiceSoapQSPort();
      if (iEmployeeUpdateService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)iEmployeeUpdateService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)iEmployeeUpdateService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (iEmployeeUpdateService != null)
      ((javax.xml.rpc.Stub)iEmployeeUpdateService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.dell.schemas.Facilities.IWMS.Employee._1_0.FacilitiesEmployeeLocationUpdate.IEmployeeUpdateService getIEmployeeUpdateService() {
    if (iEmployeeUpdateService == null)
      _initIEmployeeUpdateServiceProxy();
    return iEmployeeUpdateService;
  }
  
  public void employeeUpdate(com.dell.schemas.Facilities.IWMS.Employee._1_0.FacilitiesEmployeeLocationUpdate.FacilitiesEmployeeLocationUpdate parameters) throws java.rmi.RemoteException{
    if (iEmployeeUpdateService == null)
      _initIEmployeeUpdateServiceProxy();
    iEmployeeUpdateService.employeeUpdate(parameters);
  }
  
  
}