package com.tririga.custom.gocfi.DellOutbound;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.xml.rpc.Call;

import org.apache.axis.AxisFault;
import org.apache.axis.client.Stub;
import org.apache.log4j.Logger;

import com.dell.schemas.Facilities.IWMS.Employee._1_0.FacilitiesEmployeeLocationUpdate.EmployeeUpdateServiceSoapQSServiceLocator;
import com.dell.schemas.Facilities.IWMS.Employee._1_0.FacilitiesEmployeeLocationUpdate.FacilitiesEmployeeLocationUpdate;
import com.dell.schemas.Facilities.IWMS.Employee._1_0.FacilitiesEmployeeLocationUpdate.IEmployeeUpdateService;
import com.tririga.pub.workflow.CustomBusinessConnectTask;
import com.tririga.pub.workflow.Record;
import com.tririga.ws.dto.*;
import com.tririga.ws.TririgaWS;

public class DellEmployeeOutbound implements CustomBusinessConnectTask
{
	static Logger log = Logger.getLogger(DellEmployeeOutbound.class);
	
	//Fields to return in Dynamic Query
	
	private DisplayLabel triIdTX = new DisplayLabel("triIdTX","Badge ID","General");
	private DisplayLabel triNameTX = new DisplayLabel("triNameTX","Full Name", "General");
	private DisplayLabel cfiJackIDTX = new DisplayLabel("cfiJackIDTX","Jack Code","General");
	private DisplayLabel triMailStopTX = new DisplayLabel("triMailStopTX","Mail Stop","General");
	private DisplayLabel cfiWorkLocationCDTX = new DisplayLabel("cfiWorkLocationCDTX","Work Location Code","General");
	private DisplayLabel cfiWorkLocationEffDate = new DisplayLabel("triModifiedSY", "Work Location Eff Date", "General");
	private DisplayLabel cfiRemoteProfileTX = new DisplayLabel("cfiRemoteProfileTX","Remote Profile","General");
	private DisplayLabel cfiRemoteProfileCommentsTX = new DisplayLabel("cfiRemoteProfileCommentsTX","Remote Profile Notes","General");
	
	//Filters for Dynamic Query
	private Filter idFilter = new Filter();
	
	//Dynamic Query Properties
	private String projectName = "";
	private String moduleName = "triPeople";
	private String[] objectTypes = {"triPeople"};
	private String[] guiNames = {"cfitriEmployee"};
	private String associatedModuleName = null;
	private String associatedObjectTypeName = null;
	private int projectScope = 2;
	private DisplayLabel[] displayFields = {triIdTX, triNameTX, cfiJackIDTX, triMailStopTX, cfiWorkLocationCDTX,cfiWorkLocationEffDate, cfiRemoteProfileTX, cfiRemoteProfileCommentsTX};
	private DisplayLabel[] associatedDisplayFields = null;
	private FieldSortOrder[] fieldSortOrder = null;
	//private List<Filter> queryFilters = new ArrayList<Filter>();
	private AssociationFilter[] associationFilters = null;
	private int queryStart = 1;
	private int maxResults = 1000;
	
	public static void main(String[] args) throws Throwable
	{
		FacilitiesEmployeeLocationUpdate exportRecord = new FacilitiesEmployeeLocationUpdate();
		exportRecord.setBadge("123456");
		
		log.info("Executing Call to Dell Service");
		EmployeeUpdateServiceSoapQSServiceLocator dellService = new EmployeeUpdateServiceSoapQSServiceLocator();
		
		
		IEmployeeUpdateService port = dellService.getEmployeeUpdateServiceSoapQSPort();
		
		//log.info("Port:" + port.toString());
		
		((Stub) port)._setProperty(Call.USERNAME_PROPERTY, "TririgaUser");
		//((Stub) port)._setProperty(Call.PASSWORD_PROPERTY, "src@36a23d4"); //PROD
		((Stub) port)._setProperty(Call.PASSWORD_PROPERTY, "tririgan0npr0d"); //SIT
		//((Stub) port)._setProperty(Call.PASSWORD_PROPERTY, "BadPassword"); //BadTest for Error Handling
		//((Stub) port)._setProperty(Call.SOAPACTION_USE_PROPERTY, true);
		
		log.info(((Stub) port)._getProperty(Call.PASSWORD_PROPERTY));
		//log.info(((Stub) port)._getProperty(Call.USERNAME_PROPERTY));
		try
		{
			port.employeeUpdate(exportRecord);
		}
		catch (AxisFault ex)
		{
			String faultString = ex.getFaultString();
			if(faultString.contains("Premature"))
			{
				//DO NOTHING
			}
			else
			{
				ex.printStackTrace();
			}
		}
	}

	public boolean execute(TririgaWS triws, long arg1, Record[] records) 
	{
		idFilter.setFieldName("triRecordIdSY");
		idFilter.setOperator(Filter.OP_EQUALS);
		
		for(Record outRecord : records)
		{
			Long id = outRecord.getId();
			idFilter.setValue(id.toString());
			//queryFilters.add(idFilter);
			log.info("Output Record ID: " + outRecord.getRecordId() + " returned ID: " + id.toString());
		}
		try
		{
			Filter[] newFilter = new Filter[]{idFilter};
			log.info("Executing Dynamic Query");
			QueryResult result = triws.runDynamicQuery(projectName, moduleName, objectTypes, guiNames, associatedModuleName, associatedObjectTypeName, projectScope,
					displayFields, associatedDisplayFields, fieldSortOrder, newFilter, associationFilters, queryStart, maxResults);
			
			log.info("Checking query results");
			if(result.getTotalResults() > 0)
			{
				log.info("Building Dell Service Call");
				FacilitiesEmployeeLocationUpdate exportRecord = new FacilitiesEmployeeLocationUpdate();
				for(QueryResponseHelper helper : result.getQueryResponseHelpers())
				{
					for(QueryResponseColumn column : helper.getQueryResponseColumns())
					{
                		Calendar newDate = Calendar.getInstance();
                		String fromFormat = "MM/dd/yyyy hh:mm:ss a";
                		String toFormat = "yyyy-MM-dd'T'HH:mm:ss";
                		
                		SimpleDateFormat dateFormat = new SimpleDateFormat(fromFormat);
                		
                		Date tririgaDate = null;
                		String newDateString = null;
                		Date outDate = null;
						
		                int columnIndex = column.getIndex();
		                String columnValue;
		                if(column.getValue() == null)
		                {
		                	columnValue = "";
		                	//log.info(column.getLabel() + ": NULL");
		                }
		                else
		                {
		                	columnValue = column.getValue();
		                	//log.info(column.getLabel()+ ": " + columnValue);
		                }
		                switch(columnIndex)
		                {
		                case 0:
		                	exportRecord.setBadge(columnValue);
		                	break;
		                case 2:
		                	exportRecord.setUseCode(columnValue);
		                	break;
		                case 3:
		                	exportRecord.setMailStop(columnValue);
		                	break;
		                case 4:
		                	exportRecord.setLocationId(columnValue);
		                	break;
		                case 5:
	                		tririgaDate = dateFormat.parse(columnValue);
	                		dateFormat.applyPattern(toFormat);
	                		newDateString = dateFormat.format(tririgaDate);
	                		outDate = dateFormat.parse(newDateString);
	                		newDate.setTime(outDate);
	                		//Add one hour to timestamp for Dell IDM workaround.
	                		//newDate.add(Calendar.HOUR_OF_DAY, 1);
	                		exportRecord.setEffectiveDate(newDate);
	                		break;
		                case 6:
		                	exportRecord.setWorkProfile(columnValue);
		                	break;
		                case 7:
		                	if("NULL".equals(columnValue)||"".equals(columnValue))
		                	{
		                		exportRecord.setWorkProfileDate(null);
		                	}
		                	else
		                	{
		                		tririgaDate = dateFormat.parse(columnValue);
		                		dateFormat.applyPattern(toFormat);
		                		newDateString = dateFormat.format(tririgaDate);
		                		outDate = dateFormat.parse(newDateString);
		                		newDate.setTime(outDate);
		                		exportRecord.setWorkProfileDate(newDate);
		                	}
	                		break;		                	
		                }
					}
				}
				log.info("Executing Call to Dell Service");
				EmployeeUpdateServiceSoapQSServiceLocator dellService = new EmployeeUpdateServiceSoapQSServiceLocator();
				
				
				IEmployeeUpdateService port = dellService.getEmployeeUpdateServiceSoapQSPort();
				
				//log.info("Port:" + port.toString());
				
				((Stub) port)._setProperty(Call.USERNAME_PROPERTY, "TririgaUser");
				//((Stub) port)._setProperty(Call.PASSWORD_PROPERTY, "src@36a23d4"); //PROD
				((Stub) port)._setProperty(Call.PASSWORD_PROPERTY, "tririgan0npr0d"); //SIT
				//((Stub) port)._setProperty(Call.PASSWORD_PROPERTY, "BadPassword"); //BadTest for Error Handling
				
				port.employeeUpdate(exportRecord);

				return true;
			}
			else
			{
				log.error("Query Returned no Results");
				return false;
			}
		}
		catch (AxisFault ex)
		{
			String faultString = ex.getFaultString();
			if(faultString.contains("Premature"))
			{
				return true;
			}
			else
			{
				ex.printStackTrace();
				return false;
			}
		}
		catch (RemoteException ex)
		{
			log.error("Remote Exception");
			throw new RuntimeException(ex);
		}
		catch (Exception ex)
		{
			throw new RuntimeException(ex);
		}		
	}
}
