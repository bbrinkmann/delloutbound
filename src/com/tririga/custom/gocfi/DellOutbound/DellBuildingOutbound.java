package com.tririga.custom.gocfi.DellOutbound;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.xml.rpc.Call;

import org.apache.axis.client.Stub;
import org.apache.log4j.Logger;

import com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation.BuildingInformation;
import com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation.BuildingUpdateServiceSoapQSServiceLocator;
import com.dell.schemas.Facilities.IWMS.Building._1_0.BuildingInformation.IBuildingUpdateService;
import com.tririga.pub.workflow.CustomBusinessConnectTask;
import com.tririga.pub.workflow.Record;
import com.tririga.ws.dto.*;
import com.tririga.ws.TririgaWS;

public class DellBuildingOutbound implements CustomBusinessConnectTask
{
	static Logger log = Logger.getLogger(DellBuildingOutbound.class);
	
	//Fields to return in Dynamic Query
	
	private DisplayLabel cfiLocationIdTX = new DisplayLabel("cfiLocationIdTX","Location Code","RecordInformation");
	private DisplayLabel triModifiedSY = new DisplayLabel("triModifiedSY","Effective Date","General");
	private DisplayLabel triStatusCL = new DisplayLabel("triStatusCL","Status","General");
	private DisplayLabel triNameTX = new DisplayLabel("triNameTX","Building Name","General");
	private DisplayLabel cfiBuildingCodeTX = new DisplayLabel("cfiBuildingCodeTX","Building Code","General");
	private DisplayLabel cfiFacilityManagerOutTX = new DisplayLabel("cfiFacilityManagerOutTX","Building Manager","General");
	private DisplayLabel triBuildingClassCL = new DisplayLabel("triBuildingClassCL","Building Class","General");
	private DisplayLabel cfiAddress1TX = new DisplayLabel("cfiAddress1TX","Address Street1","RecordInformation");
	private DisplayLabel cfiAddress2TX = new DisplayLabel("cfiAddress2TX","Address Street2","RecordInformation");
	private DisplayLabel cfiAddress3TX = new DisplayLabel("cfiAddress3TX","Address Street3","RecordInformation");
	private DisplayLabel triCityTX = new DisplayLabel("triCityTX","City","General");
	private DisplayLabel triStateProvTX = new DisplayLabel("triStateProvTX","State Name","General");
	private DisplayLabel cfiStateCodeTX = new DisplayLabel("cfiStateCodeTX","State Code","RecordInformation");
	private DisplayLabel triZipPostalTX = new DisplayLabel("triZipPostalTX","Zip Code","General");
	private DisplayLabel triWorldRegionTX = new DisplayLabel("triWorldRegionTX","Region","General");
	private DisplayLabel triCountryTX = new DisplayLabel("triCountryTX","Country Name","General");
	private DisplayLabel triCountryCodeTX = new DisplayLabel("triCountryCodeTX","Country Code","General");
	private DisplayLabel triMainPhoneTX = new DisplayLabel("triMainPhoneTX","Main Phone","General");
	private DisplayLabel triMainFaxTX = new DisplayLabel("triMainFaxTX","Main Fax","General");
	private DisplayLabel triLegalNameTX = new DisplayLabel("triLegalNameTX","Legal Name","General");
	private DisplayLabel triTimeZonesCL = new DisplayLabel("triTimeZonesCL","Time Zone","General");
	private DisplayLabel triParentPropertyTX = new DisplayLabel("triParentPropertyTX","Property","General");
	
	//Filters for Dynamic Query
	private Filter idFilter = new Filter();
	
	//Dynamic Query Properties
	private String projectName = "";
	private String moduleName = "Location";
	private String[] objectTypes = {"triBuilding"};
	private String[] guiNames = {"cfitriBuilding"};
	private String associatedModuleName = null;
	private String associatedObjectTypeName = null;
	private int projectScope = 2;
	private DisplayLabel[] displayFields = {cfiLocationIdTX,triModifiedSY,triStatusCL,triNameTX,cfiBuildingCodeTX,cfiFacilityManagerOutTX,triBuildingClassCL,cfiAddress1TX,
			cfiAddress2TX,cfiAddress3TX,triCityTX,triStateProvTX,cfiStateCodeTX,triZipPostalTX,triWorldRegionTX,triCountryTX,triCountryCodeTX,triMainPhoneTX,triMainFaxTX,
			triLegalNameTX,triTimeZonesCL,triParentPropertyTX};
	private DisplayLabel[] associatedDisplayFields = null;
	private FieldSortOrder[] fieldSortOrder = null;
	//private List<Filter> queryFilters = new ArrayList<Filter>();
	private AssociationFilter[] associationFilters = null;
	private int queryStart = 1;
	private int maxResults = 1000;
	

	public boolean execute(TririgaWS triws, long arg1, Record[] records) 
	{
		idFilter.setFieldName("triRecordIdSY");
		idFilter.setOperator(Filter.OP_EQUALS);
		
		for(Record outRecord : records)
		{
			Long id = outRecord.getId();
			idFilter.setValue(id.toString());
			//queryFilters.add(idFilter);
			log.info("Output Record ID: " + outRecord.getRecordId() + " returned ID: " + id.toString());
		}
		try
		{
			Filter[] newFilter = new Filter[]{idFilter};
			log.info("Executing Dynamic Query");
			QueryResult result = triws.runDynamicQuery(projectName, moduleName, objectTypes, guiNames, associatedModuleName, associatedObjectTypeName, projectScope,
					displayFields, associatedDisplayFields, fieldSortOrder, newFilter, associationFilters, queryStart, maxResults);
			
			log.info("Checking query results");
			if(result.getTotalResults() > 0)
			{
				BuildingInformation exportRecord = new BuildingInformation();
				for(QueryResponseHelper helper : result.getQueryResponseHelpers())
				{
					for(QueryResponseColumn column : helper.getQueryResponseColumns())
					{
						
                		Calendar newDate = Calendar.getInstance();
                		String fromFormat = "MM/dd/yyyy hh:mm:ss a";
                		String toFormat = "yyyy-MM-dd'T'HH:mm:ss";
                		
                		SimpleDateFormat dateFormat = new SimpleDateFormat(fromFormat);
                		
                		Date tririgaDate = null;
                		String newDateString = null;
                		Date outDate = null;
	                	
		                int columnIndex = column.getIndex();
		                String columnValue;
		                if(column.getValue() == null)
		                {
		                	columnValue = "";
		                	//log.info(column.getLabel() + ": NULL");
		                }
		                else
		                {
		                	columnValue = column.getValue();
		                	//log.info(column.getLabel()+ ": " + columnValue);
		                }
		                log.info("Outputting Column: " + columnIndex + " Value: " + columnValue);
		                switch(columnIndex)
		                {		                	
		                	case 0:
		                		exportRecord.setLocationId(columnValue);
		                		break;
		                	case 1:
		                		tririgaDate = dateFormat.parse(columnValue);
		                		dateFormat.applyPattern(toFormat);
		                		newDateString = dateFormat.format(tririgaDate);
		                		outDate = dateFormat.parse(newDateString);
		                		newDate.setTime(outDate);
		                		exportRecord.setEffectiveDate(newDate);
		                		break;
		                	case 2:
		                		exportRecord.setStatus(columnValue);
		                		break;
		                	case 3:
		                		exportRecord.setBuildingName(columnValue);
		                		break;
		                	case 4:
		                		exportRecord.setBuildingCode(columnValue);
		                		break;
		                	case 5:
		                		exportRecord.setBuildingManager(columnValue);
		                		break;
		                	case 6:
		                		exportRecord.setBuildingType(columnValue);
		                		break;
		                	case 7:
		                		exportRecord.setAddressStreet1(columnValue);
		                		break;
		                	case 8:
		                		exportRecord.setAddressStreet2(columnValue);
		                		break;
		                	case 9:
		                		exportRecord.setAddressStreet3(columnValue);
		                		break;
		                	case 10:
		                		exportRecord.setCity(columnValue);
		                		break;
		                	case 11:
		                		exportRecord.setStateName(columnValue);
		                		break;
		                	case 12:
		                		exportRecord.setStateCode(columnValue);
		                		break;
		                	case 13:
		                		exportRecord.setZipCode(columnValue);
		                		break;
		                	case 14:
		                		exportRecord.setRegion(columnValue);
		                		break;
		                	case 15:
		                		exportRecord.setCountryName(columnValue);
		                		break;
		                	case 16:
		                		exportRecord.setCountryCode(columnValue);
		                		break;
		                	case 17:
		                		exportRecord.setPhone(columnValue);
		                		break;
		                	case 18:
		                		exportRecord.setFax(columnValue);
		                		break;
		                	case 19:
		                		exportRecord.setCompanyName(columnValue);
		                	case 20:
		                		exportRecord.setTimeZone(columnValue);
		                		break;
		                	case 21:
		                		exportRecord.setProperty(columnValue);
		                		break;
		                }
					}
				}
				
				BuildingUpdateServiceSoapQSServiceLocator dellService = new BuildingUpdateServiceSoapQSServiceLocator();
								
				IBuildingUpdateService port = dellService.getBuildingUpdateServiceSoapQSPort();
				((Stub) port)._setProperty(Call.USERNAME_PROPERTY, "TririgaUser");
				((Stub) port)._setProperty(Call.PASSWORD_PROPERTY, "src@36a23d4");
				
				port.buildingUpdate(exportRecord);
				
				return true;
			}
			else
			{
				log.info("Query Returned no Results");
				return false;
			}
		}
		catch (Exception ex)
		{
			log.warn(ex.toString());
			return false;
		}		
	}
}
